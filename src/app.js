 /** libs */
require('dotenv').config({path: 'src/.env'});  
const express = require('express');
const expressMyconnection = require('express-myconnection');
const mysql = require('mysql');
const path = require('path');
const morgan = require('morgan');
const _routes = require('./routes/animalsInfo');

/** init */
const app = express(); 

  /** Settings */
  app.set('port',process.env.PORT); 
  app.set('view engine', 'ejs');
  app.set('views', path.join(__dirname,'views'));
  
  //** Static files */
  app.use('/static', express.static(__dirname + '/public'));

/** Midlewares */
app.use(express.urlencoded({extended: false})); 
app.use(morgan('tiny'));
app.use(expressMyconnection(mysql,{
    host: process.env.DB_HOST ,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT,
    database: process.env.DB_NAMEDB,
    insecureAuth : true},'single'));
    
    /** Routes */
    app.use('/', _routes);
    
 /**Handler404 */
app.use((req,res)=> {  
    res.status(404).render('404page');
});



  /** Start */
app.listen(app.get('port'), () =>{        
    console.log(app.get('port'));  
});