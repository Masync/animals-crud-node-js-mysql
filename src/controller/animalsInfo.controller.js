const controller = {};

controller.list =  (req, res) =>{

    req.getConnection((err, conn)=>{
        conn.query('SELECT id,nameAnimal,raze,age,animalDad,animalMom,address,phone from animalsInfo', (err, info)=>{
            if (err) {
                res.json(err);
            }
            console.log(info);
            
            res.render('index',{
                data: info
            });
        });
    });
};
        controller.form = (req, res) =>{
            res.render('form');
        };
        controller.save = (req, res) =>{
            const data = req.body;
            req.getConnection((err, conn)=>{
                conn.query('INSERT INTO animalsInfo set ?', [data], (err, customers)=>{
                    if (err) {
                        res.json(err);
                    }
                    
                    res.redirect('/');
                });
            });
        };
        controller.delete = (req, res) =>{
            const {id} = req.params;
         
            req.getConnection((err, conn)=> {
                conn.query('DELETE FROM animalsInfo WHERE id = ?', [id], (err, row) => {
                    res.redirect('/');
                });
            });
         };
         controller.edit = (req, res) =>{
            const {id} = req.params;
            req.getConnection((err, conn)=> {
                conn.query('SELECT id,nameAnimal,raze,age,animalDad,animalMom,address,phone FROM animalsInfo WHERE id = ?', [id], (err, row) => {
                    res.render('edit', {
                        data: row[0]
                    });
                });
            });
        
        };
        controller.update = (req, res) =>{
            const {id} = req.params;
            const newData = req.body;
        
            req.getConnection((err, conn)=> {
                conn.query('UPDATE animalsInfo set ?  WHERE id = ?', [newData,id], (err, row) => {
                    res.redirect('/');
                });
            });
        };
    

module.exports = controller;