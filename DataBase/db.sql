
CREATE DATABASE crudanimalsmysql;

-- *using database
use crudanimalsmysql;


-- *Create table
CREATE TABLE animalsInfo
(
     id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
     nameAnimal VARCHAR(50) NOT NULL,
     raze VARCHAR(50) NOT NULL,
     age VARCHAR(50) NOT NULL,
     animalDad VARCHAR(50) NOT NULL,
     animalMom VARCHAR(50) NOT NULL,
     address VARCHAR(100) NOT NULL,
     phone VARCHAR(15) NOT NULL
);

-- show all tables
SHOW TABLES;

-- DESCRIBE TABLE
DESCRIBE animalsInfo;
-- select table
select id,nameAnimal,raze,age,animalDad,animalMom,address,phone from animalsInfo;